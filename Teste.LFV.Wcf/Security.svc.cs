﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Teste.LFV.Wcf
{

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public class Security : ISecurity
    {
        DateTime date = DateTime.MinValue;
        string token = string.Empty;
        public string Token
        {
            get
            {
                if (string.IsNullOrEmpty(token))
                {
                    token = Guid.NewGuid().ToString();
                    date = DateTime.Now;
                }
                return token;
            }
        }

        public string GenerateToken()
        {
            token = string.Empty;
            return Token;
        }

        public bool ValidToken(string token)
        {
            if (token != Token)
            {
                return false;
            }

            TimeSpan dif = DateTime.Now.Subtract(date);

            return dif.TotalSeconds <= 60;
        }

    }
}
