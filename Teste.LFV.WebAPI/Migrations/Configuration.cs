namespace Teste.LFV.WebAPI.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Teste.LFV.WebAPI.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<Teste.LFV.WebAPI.Dal.StocksContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Teste.LFV.WebAPI.Dal.StocksContext context)
        {
            context.Products.AddOrUpdate(
                    new Product { Name = "Martelo", Description = "Martelo", Price = 18.00d },
                    new Product { Name = "Chave de Fenda 3 1/8", Description = "Chave de Fenda 3 1/8", Price = 7.00d }
                );
        }
    }
}
