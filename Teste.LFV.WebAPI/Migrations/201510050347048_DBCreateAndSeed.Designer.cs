// <auto-generated />
namespace Teste.LFV.WebAPI.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class DBCreateAndSeed : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(DBCreateAndSeed));
        
        string IMigrationMetadata.Id
        {
            get { return "201510050347048_DBCreateAndSeed"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
