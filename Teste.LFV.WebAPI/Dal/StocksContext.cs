﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using Teste.LFV.WebAPI.Models;

namespace Teste.LFV.WebAPI.Dal
{
    //https://github.com/MikeWasson/BookService
    public class StocksContext: DbContext
    {
        public StocksContext()
            : base("name=StocksContext")
        {
        }
        
        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}