﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Teste.LFV.Wcf
{

    [ServiceContract(SessionMode=SessionMode.Required)]
    public interface ISecurity
    {

        [OperationContract(IsInitiating=true, IsTerminating=false)]
        string GenerateToken();

        [OperationContract(IsInitiating = true, IsTerminating = false)]
        bool ValidToken(string token);

    }
}
